package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var name1 : EditText
    private lateinit var lastname1 : EditText
    private lateinit var phone1 : EditText
    private lateinit var Pnumber1 : EditText
    private lateinit var registration1 : Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        name1 = findViewById(R.id.name)
        lastname1 = findViewById(R.id.lastname)
        phone1 = findViewById(R.id.phone)
        Pnumber1 = findViewById(R.id.Pnumber)
        registration1 = findViewById(R.id.registration)

        registration1.setOnClickListener {
            val name2 = name1.text.toString().trim()
            val lastname2 = lastname1.text.toString().trim()
            val phone2 = phone1.text.toString().trim()
            val Pnumber2 = Pnumber1.text.toString().trim()

            if (name2.isEmpty() || name2.length < 2) {
                name1.error = "შეიყვანეთ სახელი"
                return@setOnClickListener

            } else if (lastname2.isEmpty() || lastname2.length < 5) {
                lastname1.error = "შეიყვანეთ გავრი"
                return@setOnClickListener
            } else if (phone2.isEmpty() || phone2.length != 9) {
                phone1.error = "შეიყვანეტ ნომერი"
                return@setOnClickListener
            } else if (Pnumber2.isEmpty() || Pnumber2.length != 11) {
                phone1.error = "შეიყვანეტ პირადი ნომერი"
                return@setOnClickListener
            } else {
                Toast.makeText(this, "წარმატებით გაიარეთ", Toast.LENGTH_SHORT).show()


            }
        }
    }
}